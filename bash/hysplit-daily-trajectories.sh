#!/bin/bash -   
#title          :hysplit-daily-trajectories.sh
#description    :Daily backward and forward trajectories with HYSPLIT model
#author         :Giovanni Bonafe'
#date           :20220705
#version        :1.4
#usage          :./hysplit-daily-trajectories.sh
#notes          :       
#bash_version   :4.2.46(2)-release
#============================================================================

.  ~/.bash_profile

# parameters
FTP_TRANSFER_UTILITY="ncftpput"     # The utility to be used for file transfer
FTP_SERVER_NAME="193.43.169.198"    # The server name
FTP_USERNAME="crma"                 # User name required for file transfer
FTP_PASSWORD="crma2012"             # Password required  for file transfer
FTP_REMOTE_AREA="AA08/daily"        # Remote area where files are uploaded

# paths
root_dir="/u/arpa/bonafeg/src/hysplit-backtrajectories"
R_dir=${root_dir}"/R"
out_dir=${root_dir}"/out"
log_dir=${root_dir}"/log"
bash_dir=${root_dir}"/bash"
met_dir=${root_dir}"/data/met"
traj_dir=${root_dir}"/data/traj"
pbs_file=${root_dir}/hysplit_daily_traj.pbs
pub_dir="/u/arpa/bonafeg/public_html/hysplit_traj"

# clean
find ${met_dir}* -mtime +10 -exec rm {} \;
find ${traj_dir}* -mtime +10 -exec rm {} \;

# prepare PBS
cat << EOF > ${pbs_file}
#PBS -l walltime=04:00:00
#PBS -l select=1:ncpus=32
#PBS -V
#PBS -q hp

.  ~/.bash_profile
module purge
module load r-rgdal

# calculate trajectories with HYSPLIT
cd ${root_dir}
Rscript ${R_dir}/calc_trajectories.R
logger -s -t "INFO [$(date -u +'%Y-%m-%d %H:%M:%S')]" "Trajectories calculated"
	
# plot trajectories
rm ${out_dir}/*
logger -s -t "INFO [$(date -u +'%Y-%m-%d %H:%M:%S')]" "Plotting"
Rscript ${R_dir}/plot_trajectories.R
logger -s -t "INFO [$(date -u +'%Y-%m-%d %H:%M:%S')]" "Trajectories plotted"

# create index.html
Rscript ${R_dir}/prepare_indexhtml.R

# copy to public_html
cp ${out_dir}/*.* ${pub_dir}
 
# upload to FTP
cd ${out_dir}
${FTP_TRANSFER_UTILITY} -u ${FTP_USERNAME} -p ${FTP_PASSWORD} ${FTP_SERVER_NAME} ${FTP_REMOTE_AREA} *.*
logger -s -t "INFO [$(date -u +'%Y-%m-%d %H:%M:%S')]" "Plots uploaded"
EOF
qsub -N hysplit_traj -e ${log_dir} -o ${log_dir} -j eo ${pbs_file}
