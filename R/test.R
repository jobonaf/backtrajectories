library(SplitR)
coo <- c(45.8, 13.5333) # Monfalcone
run_period <- c("2014-01-01","2016-12-31")
tMNF <- hysplit_trajectory(
  lat = coo[1],
  lon = coo[2],
  height = 500,
  duration = 48,
  run_period = run_period,
  daily_hours = c(12),
  direction = "backward",
  met_type = "reanalysis",
  extended_met = FALSE)

if(!dir.exists("all")) dir.create("all")
system("rm all/*")
ff <- dir(recursive = T, pattern = "48h", full.names = T)
file.link(ff,paste0("all/",basename(ff)))
tMNF <- trajectory_read("all")
save(tMNF,file = "tMNF.rda")

library(openair)
ncl<-min(7, round(length(unique(tMNF$date))/7))
pdf("trajCluster.pdf", height=5, width=8)
clu <- trajCluster(tMNF, projection = "lambert", parameters = coo, n.cluster = ncl, 
                   cols = "Set1", method = "Euclid", grid.col = "transparent",
                   xlab="", ylab="", orientation = c(90,0,coo[2]))
dev.off()

clu <- clu$data
save(clu,file = "clu_tMNF.rda")

library(dplyr)
pdf("calendarPlot.pdf", onefile = T)
years <- unique(format(seq.Date(as.Date(run_period[1]), 
                                as.Date(run_period[2]), 
                                by = "1 week"), "%Y"))
for (yy in years) {
  calendarPlot(clu %>% 
                 filter(format(date,"%Y")==yy) %>%
                 mutate(cluster=as.numeric(substr(cluster,2,2))), 
               pollutant = "cluster", year = as.numeric(yy), cols = "Set1", 
               breaks = 0.5:(ncl+0.5), labels = paste0("C",1:ncl))
}
dev.off()

write.csv(clu %>% mutate(date=format(date,"%Y-%m-%d")) %>% group_by(date) %>% summarise(cluster=first(cluster)), 
          file = "cluster_tMNF.csv", row.names = F, quote=F)

# pdf("trajPlot.pdf")
# trajPlot(mydata = data.frame(tMNF, cluster=clu$cluster), 
#          group = "cluster", map.cols = "grey70", grid.col = "transparent", 
#          npoints = 24, cols="Set1")
# dev.off()

