HYSPLIT trajectories
====
R and bash scripts to manage HYSPLIT trajectories

### Author

[Giovanni Bonafè](mailto:giovanni.bonafe@arpa.fvg.it)


### Content

|directory | content                       |gitted |
|:---------|:------------------------------|:------|
|bash      |bash scripts                   |  Y    |
|data      |coordinates of start/end points|  Y    |
|data/met  |meteorological input           |  N    |
|data/traj |trajectories in R format       |  N    |
|log       |log                            |  N    |
|out       |maps and charts                |  N    |
|R         |R scripts                      |  Y    |
|run       |work directory                 |  N    |
|tmp       |temporary directory            |  N    |
